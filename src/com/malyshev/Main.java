package com.malyshev;

import java.util.HashSet;
import java.util.Random;

public class Main {

  private static boolean running = true;
  private static Object locker = new Object();
  private static HashSet uniqueNumbers = new HashSet(100);

  public static void main(String[] args) {

    Thread randThr = new Thread(new Runnable() {
      @Override
      public void run() {
        while (running) {
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          int min = 0;
          int max = 99;
          int randNumber = min + (int) (Math.random() * (max + 1));
          synchronized (locker) {
            uniqueNumbers.add(randNumber);
            if (uniqueNumbers.size() == (max + 1)) {
              running = false;
              System.out.println("--- Generated all possible " + "(" + uniqueNumbers.size() + ")"  + " unique numbers ---");
            }
            locker.notifyAll();
          }
          if (running)
            System.out.println(randNumber);
        }
      }
    });

    Thread countThr = new Thread(new Runnable() {
      @Override
      public void run() {
        int j = 1;
        while (running) {
          synchronized (locker) {
            try {
              locker.wait();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          if (j%5 == 0 && running) {
            System.out.println("--- Generated " + uniqueNumbers.size() + " unique numbers ---");
            j = 0;
          }
          j++;
        }
      }
    });

    randThr.start();
    countThr.start();
  }
}
